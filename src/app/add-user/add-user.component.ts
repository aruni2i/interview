import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { validateEmail, validateAlphabet, validateUser, validatePassword, validateMobile } from '../validators/validate';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  private submitted : boolean;
  @Input() addUserData:any;
  @Output() addClk = new EventEmitter<any>();
  addUserForm = this.frm.group({

    firstName: ["", [Validators.required, validateAlphabet]],
    lastName: ["", [Validators.required, validateAlphabet]],
    gender: ["male", [Validators.required]],
    email: ["", [Validators.required, validateEmail]],
    mobile: ["", [Validators.required, validateMobile]],
    userName: ["", [Validators.required, validateUser]],
    city: [null, [Validators.required]],
    isSelected: ["false", [Validators.required]]

  });
  
  constructor(private frm: FormBuilder) { }

  ngOnInit() {
  }

  private addUser() {
    this.submitted=true;
    console.log(this.addUserForm);
    if(this.addUserForm.valid){
      this.addClk.emit(this.addUserForm.value);
    }
   
  }

}
