import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import { GridComponent } from './grid/grid.component';
import { HeaderComponent } from './header/header.component';
import { AddUserComponent } from './add-user/add-user.component';
import { ModalConfigComponent } from './modal-config/modal-config.component';


import { RestApiService } from 'src/service/rest-api.service';
import { Guard } from './validators/gaurd';

@NgModule({
  declarations: [
    UsersComponent,
    GridComponent,
    HeaderComponent,
    AddUserComponent,
    ModalConfigComponent,
    LoginComponent,
    RegisterComponent
  ]
})
export class AdminModule { }
