import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModalConfigComponent } from './modal-config/modal-config.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { RegisterComponent } from './register/register.component';
import { Guard } from './validators/gaurd';
import { CandidateLoginComponent } from './candidate-login/candidate-login.component';
import { CandidatePaperComponent } from './candidate-paper/candidate-paper.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: '', component: UsersComponent, canActivate: [Guard]},
  { path: 'modal', component: ModalConfigComponent, canActivate: [Guard]},
  { path: 'candidate-login', component: CandidateLoginComponent},
  { path: 'candidate-paper', component: CandidatePaperComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
