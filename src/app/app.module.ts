import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AdminModule } from './admin.module';
import { AppRoutingModule } from './app-routing.module';

import { Guard } from './validators/gaurd';
import { RestApiService } from 'src/service/rest-api.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import { GridComponent } from './grid/grid.component';
import { HeaderComponent } from './header/header.component';
import { AddUserComponent } from './add-user/add-user.component';
import { ModalConfigComponent } from './modal-config/modal-config.component';
import { CandidateLoginComponent } from './candidate-login/candidate-login.component';
import { CandidatePaperComponent } from './candidate-paper/candidate-paper.component';
import { PopoverBasicComponent } from './popover-basic/popover-basic.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    GridComponent,
    HeaderComponent,
    AddUserComponent,
    ModalConfigComponent,
    LoginComponent,
    RegisterComponent,
    CandidateLoginComponent,
    CandidatePaperComponent,
    PopoverBasicComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
    
  ],
  providers: [RestApiService,Guard],
  bootstrap: [AppComponent]
})
export class AppModule { }
