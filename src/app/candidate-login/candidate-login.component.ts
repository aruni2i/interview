import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from '../../service/rest-api.service';
import { validateEmail, validatePassword } from '../validators/validate';

@Component({
  selector: 'app-candidate-login',
  templateUrl: './candidate-login.component.html',
  styleUrls: ['./candidate-login.component.scss']
})
export class CandidateLoginComponent implements OnInit {

  private loginForm: FormGroup;
  private isSubmitted = false;
  private addUserData ={
    selectBtn : false,
    btnName : "Login",
    btnClass:""
  };
  // img1 = require("../../assets/profilePlaceholder4.png");

  constructor(private api: RestApiService, private router: Router, private formBuilder: FormBuilder) { }
  submitted = false
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }

  get g() { 
    return this.loginForm.controls;
   }

  login() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.api.get('/users?email='+this.loginForm.value.email,(data : any) => {
        if(data.length==0){
          sessionStorage.setItem('token', 'candidate-key');
          this.router.navigateByUrl('');
        }
      });
    }
  }
  addCandidate(data : any){
    sessionStorage.setItem("candidate-details",JSON.stringify(data));
    this.router.navigateByUrl('/candidate-paper');
  }
} 




