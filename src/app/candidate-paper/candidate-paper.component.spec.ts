import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatePaperComponent } from './candidate-paper.component';

describe('CandidatePaperComponent', () => {
  let component: CandidatePaperComponent;
  let fixture: ComponentFixture<CandidatePaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatePaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatePaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
