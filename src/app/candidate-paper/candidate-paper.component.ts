import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from 'src/service/rest-api.service';

@Component({
  selector: 'app-candidate-paper',
  templateUrl: './candidate-paper.component.html',
  styleUrls: ['./candidate-paper.component.scss']
})
export class CandidatePaperComponent implements OnInit, AfterViewInit {

  private questions : Array<any>;
  private questionForm : FormGroup;
  private mark : number;
  private percentage: number;
  private candidateDtls: any;
  private currentPage : number = 0;
  private modalBtnName = "Submit";
  constructor(private formBuilder : FormBuilder, private router : Router, private api : RestApiService) { }

  ngOnInit() {
    this.questions = [
      {
        question : "What does CSS stand for?",
        options :[
          "Combined Style Sheets", 
          "Compounding Style Sheets", 
          "Cascading Style Sheets",
          "Casting Style Sheets"
        ],
        answer:'c'
      },{
        question : "Which HTML attribute is used to define inline styles?",
        options :[
          "Class", 
          "Style", 
          "Id",
          "Name"
        ],
        answer:'b'
      },{
        question : "Which is the correct CSS syntax?",
        options :[
          "body { color : black; }", 
          "body { color == black };", 
          "body { color ; black; }",
          "body { color = black }"
        ],
        answer:'a'
      },{
        question : "What does CSS stand for?",
        options :[
          "Combined Style Sheets", 
          "Compounding Style Sheets", 
          "Cascading Style Sheets",
          "Casting Style Sheets"
        ],
        answer:2
      },{
        question : "Which HTML attribute is used to define inline styles?",
        options :[
          "Class", 
          "Style", 
          "Id",
          "Name"
        ],
        answer:'b'
      }
    ];
    this.questionForm = this.formBuilder.group({
      0 : "",
      1 : "",
      2 : "",
      3 : "",
      4 : "",
    });    
  }
  ngAfterViewInit(){
   
  }
  private submit(data){
    this.mark=0;
    for(let i=0;i<this.questions.length;i++){
      if(this.questions[i].answer===data[i])
      {
        this.mark++;
      }
    }
    this.percentage=(this.mark/this.questions.length*100);
    this.filterCandidate(this.percentage);
  }

  private filterCandidate(data : number){

     this.candidateDtls=JSON.parse(sessionStorage.getItem('candidate-details'));
     if(data>=50){
      this.candidateDtls.isSelected = "true";
     }
     else
     {
       this.candidateDtls.isSelected= "false";
     }
     this.addCandidate(this.candidateDtls);
  }

  private addCandidate(data: any){
    this.api.post('/candidates',data,(data : any) => {
      sessionStorage.removeItem('candidate-details');
      document.getElementById("modalOpen").click();
    });
  }

  private next(){
    this.currentPage++;
  }
  private prev(){
    this.currentPage--;
  }

  private logOut(){
    document.getElementById("clsBtn").click();
    this.router.navigateByUrl('/candidate-login');
  }

  // document.getElementById("clsBtn").click();
  // document.getElementById("modalOpen").click();

  

} 
