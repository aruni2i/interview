import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  @Input() data : any; 
  @Output() deleteEvnt = new EventEmitter<number>();
  @Output() selectEvnt = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }
  private delete(id : number){
    this.deleteEvnt.emit(id);
  }

  private select(id : number){
    this.selectEvnt.emit(id);
  }

}
