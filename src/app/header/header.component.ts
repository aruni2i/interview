import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isCollapsed = true;
  @Output() add = new EventEmitter<any>();
  constructor(private router :Router) { }

  ngOnInit() {
  }
  // private addUser(){
  //   this.add.emit();
  // }
  private logout() {
    sessionStorage.removeItem('token')
    this.router.navigate(['/login']);
  }
}
