import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from '../../service/rest-api.service';
import { validateEmail, validatePassword } from '../validators/validate';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;
  // img1 = require("../../assets/profilePlaceholder4.png");

  constructor(private api: RestApiService, private router: Router, private formBuilder: FormBuilder) { }
  submitted = false
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get g() { 
    return this.loginForm.controls;
   }

  login() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.api.get('/users?email='+this.loginForm.value.email+'&password='+this.loginForm.value.password,(data : any) => {
        if(data.length==1){
          sessionStorage.setItem('token', 'key');
          this.router.navigateByUrl('');
        }
      });
    }
  }

  register() {
    this.router.navigate(["/register"]);
  }

}
