import { Component, OnInit, Input } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-config',
  templateUrl: './modal-config.component.html',
  styleUrls: ['./modal-config.component.scss']
})
export class ModalConfigComponent implements OnInit {

  @Input() data;
  @Input() modalBtnName;
  @Input() showHeader;
  constructor(config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
  }
  
  // openModal(){
  //   console.log("modal form modal works");
  //   var element = document.getElementById("clsBtn") as any;
  //   element.click();
  // }

  open(content) {
    this.modalService.open(content);
  }
}
