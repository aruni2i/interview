import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from '../../service/rest-api.service';
import { validateEmail, validatePassword } from '../validators/validate';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  registerForm: FormGroup;
  isSubmitted = false;
  // img1 = require("../../assets/profilePlaceholder4.png");

  constructor(private api: RestApiService, private router: Router, private formBuilder: FormBuilder) { }
  submitted = false
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get g() { 
    return this.registerForm.controls;
   }

   register() {
    this.submitted = true;
    if (this.registerForm.valid) {
      this.api.post('/users',this.registerForm.value,(data : any) => {
          this.router.navigateByUrl('/login');
      });
    }
  }
}
