import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RestApiService } from 'src/service/rest-api.service';
import { ModalConfigComponent } from '../modal-config/modal-config.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  private users: any;
  private selectedUsers : any;
  private unSelectedUsers : any;
  private head : Array<String>;
  private selectBtn : any;
  private deleteBtn : any;
  private userData : any;
  private selectedUsersGrid : any ={header:[],data:[],btn:{}};
  private unSelectedUsersGrid : any ={header:[],data:[],btn:{}};
  private modalBtnName:any;
  private addUserData ={
    selectBtn : true,
    btnName : "Add User",
    btnClass:""
  };

  constructor( private api : RestApiService) { }

  ngOnInit() {
    this.modalBtnName="Add User";
    this.selectBtn={name:"Select",class:"btn btn-success",fn:"select"};
    this.deleteBtn={name:"Delete",class:"btn btn-danger",fn:"delete"};
    this.head = ['ID', 'First Name', 'Last Name', 'User Name', 'Email', 'Mobile', 'City', 'Gender', 'Action' ];
    this.getUsers();
  }

  // btnTest() {
  //   console.log(this.myDiv.nativeElement.value);
    
  //   this.myDiv.nativeElement.value="abcd"; 
  // }
  
   
  private getUsers(){
    this.api.get('/candidates', (data : Array<any>) => {
      this.users = data;
      this.selectedUsers=this.selectedUsersFilter(this.users);
      this.unSelectedUsers=this.unSelectedUsersFilter(this.users);
      this.selectedUsersGrid = {
        title:"Selected Users",
        data : this.selectedUsers,
        header : this.head,
        btn  : this.deleteBtn
      };
      this.unSelectedUsersGrid = {
        title:"Users",
        data : this.unSelectedUsers,
        header : this.head,
        btn  : this.selectBtn
      };
    });
  }
  private addUser(data : any){
    console.log("adduser form user comp works");
    this.api.post('/candidates/', data,(data:any) => {
      console.log(data);
      document.getElementById("clsBtn").click();
      this.getUsers();
    });
  }

  private addUserModal(){
    // this.child.open();
    console.log("addusermodal fn from users comp works");

  }
  
  private select(id : number){
    console.log("select from user works "+id);
    this.userData= this.particularUser(this.users,id);
    this.userData[0].isSelected = "true";
    this.api.put('/candidates/'+id,this.userData[0],(data : Object) => {
      console.log(data);
      this.getUsers();
    });
  }

  private delete(id : number){
    this.api.delete('/candidates/'+id,(data : Object) => {
      console.log("deleted sucessfully "+id);
      this.getUsers();
    });
  }
  private particularUser(data : Array<any> , id : number) {
    return data.filter(data => data.id == id );
  }
  private selectedUsersFilter(data : Array<any>) {
    return data.filter(data => data.isSelected == "true");
  }
  private unSelectedUsersFilter(data : Array<any>) {
    return data.filter(data => data.isSelected == "false");
  }

 
}

