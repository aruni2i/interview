import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router'

@Injectable()
export class Guard {

    constructor(private router: Router) { }

    canActivate() {
        if (sessionStorage.getItem('token')=='key'){
            return true;
        }
        else{
            this.router.navigate(['/login'])
            return false;
        }
    }
}
