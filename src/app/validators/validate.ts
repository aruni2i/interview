import { AbstractControl, ValidatorFn, FormGroup } from "@angular/forms";

export function validateEmail(control: AbstractControl): { [key: string]: any } | null {

   const valid = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(control.value);
   if (control.value === '') {
      return null;
   }
   return valid ? null : { invalidEmail: { valid: false, value: control.value } };


}
export function validateAlphabet(control: AbstractControl): { [key: string]: any } | null {

   const valid = /^[A-Za-z]+$/.test(control.value);
   if (control.value === '') {
      return null;
   }
   return valid ? null : {
      invalidAlphabet: { valid: false, value: control.value }
   };


}
export function validateUser(control: AbstractControl): { [key: string]: any } | null {

   const valid = /^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/.test(control.value);
   if (control.value === '') {
      return null;
   }
   return valid ? null : { invalidUser: { valid: false, value: control.value } };


}

export function validatePassword(control: AbstractControl): { [key: string]: any } | null {

   const valid = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(control.value);
   if (control.value === '') {
      return null;
   }
   return valid ? null : { invalidUser: { valid: false, value: control.value } };


}

export function validateMobile(control: AbstractControl): { [key: string]: any } | null {

   const valid = /^[0-9]{10}$/.test(control.value);
   if (control.value === '') {
      return null;
   }
   return valid ? null : { invalidUser: { valid: false, value: control.value } };


}
