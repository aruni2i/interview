import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
  })

export class RestApiService {
    baseUrl: string = "http://192.168.1.195:3000";

    constructor( private httpClient : HttpClient ) { }

    get(url: string, callback: Function) {
        return this.httpClient.get(this.baseUrl + url).subscribe(data => {
            callback(data);
        }, error => {
            console.log(error);
        });

    }

    post(url: string, params: Object, callback: Function) {

        this.httpClient.post(this.baseUrl + url, params).subscribe(data => {
            callback(data);
        }, error => {
            console.log(error);
        });
    }


    put(url: string, params: Object, callback: Function) {

        return this.httpClient.put(this.baseUrl + url, params).subscribe(data => {
            callback(data);
        }, error => {
            console.log(error);
        });
    }


    delete(url: string, callback: Function) {
        this.httpClient.delete(this.baseUrl + url).subscribe(data => {
            callback(data);
        }, error => {
            console.error(error);
        });
    }
}


